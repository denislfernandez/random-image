import 'dart:math';
import 'dart:typed_data';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  double getScreenWidth(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  double getScreenHeight(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }

  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final List<Color> colors = [
    Colors.red[900],
    Colors.blue[900],
    Colors.deepPurple,
    Colors.green[900],
    Colors.amber[800]
  ];
  Color currentColor;
  Color nextColor;
  Image currentImage;
  String currentImageUrl = 'https://picsum.photos/1440';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Random Image',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: currentColor,
          title: Text('Random Image'),
        ),
        body: Container(
          color: Colors.blueGrey[900],
          child: InkWell(
              splashColor: nextColor,
              onTap: () {
                setState(() {
                  currentColor = nextColor;
                  nextColor = getRandomColor();
                  currentImageUrl =
                      '$currentImageUrl?v=${DateTime.now().millisecondsSinceEpoch}';
                });
              },
              child: Center(
                child: Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    FadeInImage.memoryNetwork(
                      placeholder: Uint8List(512 * 512),
                      image: currentImageUrl,
                      fadeOutDuration: Duration(milliseconds: 0),
                    ),
                  ],
                ),
              )),
        ),
      ),
    );
  }

  Color getRandomColor() {
    Color randomColor = currentColor;
    while (randomColor == currentColor) {
      randomColor = colors.elementAt(Random().nextInt(colors.length));
    }
    return randomColor;
  }
}
